package ru.batrdmi.powermouse;

import com.intellij.ide.DataManager;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.event.EditorMouseEvent;
import com.intellij.openapi.editor.event.EditorMouseEventArea;
import com.intellij.openapi.editor.event.EditorMouseListener;
import com.intellij.openapi.editor.ex.EditorEx;
import com.intellij.openapi.editor.ex.EditorPopupHandler;
import com.intellij.openapi.editor.impl.EditorImpl;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.wm.impl.IdeGlassPaneImpl;
import com.intellij.util.ReflectionUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.List;

@State(name = "PowerMouse", storages = @Storage("other.xml"))
public class EventDispatcher implements PersistentStateComponent<EventDispatcher.Settings>, Disposable {
    private static final Key<Boolean> POPUP_HANDLER_INSTALLED = Key.create("powerMouse.popupHandlerInstalled");
    private static final int PRESS_AND_HOLD_DELAY = 400;

    @SuppressWarnings("Convert2Lambda") // this guarantees handler's identity
    private final EditorPopupHandler emptyPopupHandler = new EditorPopupHandler() {
        @Override
        public boolean handlePopup(@NotNull EditorMouseEvent editorMouseEvent) {
            return true;
        }
    };

    private Settings settings = new Settings();
    private MouseEvent rightButtonPressedEvent;

    private final Timer popupMenuTimer = new Timer(PRESS_AND_HOLD_DELAY,
            e -> ApplicationManager.getApplication().invokeLater(() -> {
                if (rightButtonPressedEvent != null && settings.actionIdToExecuteOnRightClick != null) {
                    runAction(rightButtonPressedEvent, settings.actionIdToExecuteOnRightClick);
                }
                rightButtonPressedEvent = null;
            })) {{ setRepeats(false); }};

    public EventDispatcher() {
        EditorFactory.getInstance().getEventMulticaster().addEditorMouseListener(new MyEditorMouseAdapter(), this);
    }

    @Override
    public void dispose() {
    }

    private static void runAction(MouseEvent me, String actionId) {
        if (actionId == null) {
            return;
        }
        ActionManager actionManager = ActionManager.getInstance();
        AnAction action = actionManager.getAction(actionId);
        if (action == null) {
            return;
        }
        DataContext dataContext = getDataContext(me);
        Project project = CommonDataKeys.PROJECT.getData(dataContext);
        if (project != null && DumbService.isDumb(project) && !action.isDumbAware()) {
            return;
        }
        Presentation presentation = action.getTemplatePresentation().clone();
        AnActionEvent e = new AnActionEvent(me, dataContext, ActionPlaces.MAIN_MENU, presentation, actionManager,
                me.getModifiers());
        action.update(e);
        if (e.getPresentation().isEnabled()) {
            action.actionPerformed(e);
        }
    }

    private static DataContext getDataContext(MouseEvent me) {
        Component component = SwingUtilities.getDeepestComponentAt(me.getComponent(), me.getX(), me.getY());
        if (component instanceof IdeGlassPaneImpl) component = ((IdeGlassPaneImpl)component).getTargetComponentFor(me);
        return DataManager.getInstance().getDataContext(component);
    }

    private void showEditorPopupMenu(Editor editor, EditorMouseEvent e) {
        //noinspection unchecked
        List<EditorPopupHandler> popupHandlers = ReflectionUtil.getField(EditorImpl.class, editor, List.class, "myPopupHandlers");
        if (popupHandlers != null) {
            for (int i = popupHandlers.size() - 1; i >= 0; i--) {
                if (popupHandlers.get(i).handlePopup(e)) break;
            }
        }
    }

    static Settings getSettings() {
        EventDispatcher eventDispatcher = ApplicationManager.getApplication().getComponent(EventDispatcher.class);
        assert eventDispatcher != null;
        return eventDispatcher.getState();
    }

    @Nullable
    @Override
    public Settings getState() {
        return settings;
    }

    @Override
    public void loadState(@NotNull Settings settings) {
        this.settings = settings;
    }

    private class MyEditorMouseAdapter implements EditorMouseListener {
        @Override
        public void mousePressed(@NotNull EditorMouseEvent e) {
            if (e.getArea() != EditorMouseEventArea.EDITING_AREA) return;

            MouseEvent me = e.getMouseEvent();
            if (me.getButton() == MouseEvent.BUTTON3) {
                EditorEx editor = (EditorEx) e.getEditor();
                if (editor.getUserData(POPUP_HANDLER_INSTALLED) == null) {
                    editor.installPopupHandler(emptyPopupHandler);
                    // using an additional field so that handler is not installed multiple times
                    // in case it's not removed due to some bug
                    editor.putUserData(POPUP_HANDLER_INSTALLED, Boolean.TRUE);
                }
            }
            if ((me.getButton() == MouseEvent.BUTTON3 && (me.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK) != 0)
                    || (me.getButton() == MouseEvent.BUTTON1 && (me.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK) != 0)) {
                rightButtonPressedEvent = null;
                runAction(me, settings.actionIdToExecuteOnPowerClick);
            }
            else if (me.getButton() == MouseEvent.BUTTON3) {
                rightButtonPressedEvent = me;
                popupMenuTimer.restart();
            }
        }

        @Override
        public void mouseReleased(@NotNull EditorMouseEvent e) {
            if (e.getArea() != EditorMouseEventArea.EDITING_AREA) return;

            boolean wasPressed = rightButtonPressedEvent != null;
            rightButtonPressedEvent = null;

            MouseEvent me = e.getMouseEvent();
            if (me.getButton() == MouseEvent.BUTTON3) {
                EditorEx editor = (EditorEx) e.getEditor();
                editor.uninstallPopupHandler(emptyPopupHandler);
                editor.putUserData(POPUP_HANDLER_INSTALLED, null);

                if (wasPressed) {
                    showEditorPopupMenu(editor, e);
                }
                e.consume();
            }
        }
    }

    @SuppressWarnings("WeakerAccess") // make fields available for serialization
    static class Settings {
        public String actionIdToExecuteOnRightClick = "FileStructurePopup";
        public String actionIdToExecuteOnPowerClick = "ShowUsages";
    }
}
