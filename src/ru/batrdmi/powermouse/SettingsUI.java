package ru.batrdmi.powermouse;

import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.ui.ComboBox;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SettingsUI implements Configurable {
    private ComboBox<Action> rightComboBox;
    private ComboBox<Action> powerComboBox;

    @Nls
    @Override
    public String getDisplayName() {
        return "PowerMouse";
    }

    @Nullable
    @Override
    public String getHelpTopic() {
        return null;
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.WEST;
        panel.add(new JLabel("Right button action: "), c);
        Action[] actions = getAllActions();
        Action[] rightActions = new Action[actions.length + 1];
        System.arraycopy(actions, 0, rightActions, 1, actions.length);
        rightActions[0] = new Action(null, "Context menu");
        panel.add(rightComboBox = new ComboBox<>(rightActions), c);
        c.weightx = 1;
        panel.add(new JPanel(), c);
        c.weightx = 0;
        c.gridx = 0;
        c.gridy = 1;
        panel.add(new JLabel("Left+right button action: "), c);
        c.gridx = 1;
        Action[] powerActions = new Action[actions.length + 1];
        System.arraycopy(actions, 0, powerActions, 1, actions.length);
        powerActions[0] = new Action(null, "No action");
        panel.add(powerComboBox = new ComboBox<>(powerActions), c);
        c.gridx = 2;
        c.weightx = 1;
        panel.add(new JPanel(), c);
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 3;
        c.weighty = 1;
        panel.add(new JPanel(), c);
        return panel;
    }

    @Override
    public boolean isModified() {
        Action rightAction = (Action) rightComboBox.getSelectedItem();
        Action powerAction = (Action) powerComboBox.getSelectedItem();
        return rightAction != null && !Objects.equals(EventDispatcher.getSettings().actionIdToExecuteOnRightClick, rightAction.id)
                || powerAction != null && !Objects.equals(EventDispatcher.getSettings().actionIdToExecuteOnPowerClick, powerAction.id);
    }

    @Override
    public void apply() {
        Action rightAction = (Action) rightComboBox.getSelectedItem();
        if (rightAction != null) {
            EventDispatcher.getSettings().actionIdToExecuteOnRightClick = rightAction.id;
        }
        Action powerAction = (Action) powerComboBox.getSelectedItem();
        if (powerAction != null) {
            EventDispatcher.getSettings().actionIdToExecuteOnPowerClick = powerAction.id;
        }
    }

    @Override
    public void reset() {
        String rightActionId = EventDispatcher.getSettings().actionIdToExecuteOnRightClick;
        for (int i = 0; i < rightComboBox.getItemCount(); i++) {
            Action item = rightComboBox.getItemAt(i);
            if (Objects.equals(rightActionId, item.id)) {
                rightComboBox.setSelectedItem(item);
                break;
            }
        }
        String powerActionId = EventDispatcher.getSettings().actionIdToExecuteOnPowerClick;
        for (int i = 0; i < powerComboBox.getItemCount(); i++) {
            Action item = powerComboBox.getItemAt(i);
            if (Objects.equals(powerActionId, item.id)) {
                powerComboBox.setSelectedItem(item);
                break;
            }
        }
    }

    @Override
    public void disposeUIResources() {
        rightComboBox = null;
        powerComboBox = null;
    }

    private Action[] getAllActions() {
        ActionManager actionManager = ActionManager.getInstance();
        List<String> actionIds = actionManager.getActionIdList("");
        List<Action> actions = new ArrayList<>(actionIds.size());
        Map<String, Action> actionMap = new HashMap<>();
        for (String actionId : actionIds) {
            if (actionId == null) {
                continue;
            }
            AnAction action = actionManager.getAction(actionId);
            if (action == null) {
                continue;
            }
            String description = action.getTemplatePresentation().getText();
            if (description == null || description.isEmpty()) {
                continue;
            }
            Action entry = new Action(actionId, description);
            actions.add(entry);
            Action prevEntry = actionMap.put(description, entry);
            if (prevEntry != null) {
                entry.showId = true;
                prevEntry.showId = true;
            }
        }
        Collections.sort(actions);
        return actions.toArray(new Action[0]);
    }

    private static class Action implements Comparable<Action> {
        private final String id;
        private final String description;
        private boolean showId;

        private Action(String id, @NotNull String description) {
            this.id = id;
            this.description = description;
        }

        @Override
        public String toString() {
            return showId ? description + " (" + id + ")" : description;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Action action = (Action) o;
            return Objects.equals(id, action.id);
        }

        @Override
        public int compareTo(@NotNull Action o) {
            return toString().compareTo(o.toString());
        }
    }
}
