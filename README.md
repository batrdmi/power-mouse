Plugin allows you to map an action of your choice to the following mouse gestures performed in editor:

* press-and-hold of right mouse button (bound to "Quick Documentation" by default)
* simultaneous press of left and right mouse buttons (bound to "Show Usages" by default)

Plugin can be downloaded from its [page at JetBrains site](https://plugins.jetbrains.com/plugin/7904) or via IDE's plugin manager.